#!/bin/bash

wd=`pwd`
base=`basename "$wd"`

if [ -z $1 ]; then
  echo "Indique um conteiner da lista"
  bash dklist.sh | awk '{ print $2 }' | awk -F_ '{ print $2 }'
  exit 1
fi

aux=`bash dklist.sh | grep $1 | awk '{ print $1 }'`
if [ -z "$aux" ]; then
  echo "KD o container $1?"
else
  docker exec -i -t $aux /bin/bash
fi