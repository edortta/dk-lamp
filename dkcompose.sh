#!/bin/bash
dc=`which docker-compose`
if [ ! -f $dc ]; then
  dc="docker compose"
fi
$dc up -d