#!/bin/bash

#!/bin/bash

wd=`pwd`
base=`basename "$wd"`

if [ -z $1 ]; then
  echo "Please, indicate a container from the list"
  bash dklist.sh | awk '{ print $2 }' | awk -F_ '{ print $2 }'
  exit 1
fi

aux=`bash dklist.sh | grep $1 | awk '{ print $1 }'`
if [ -z "$aux" ]; then
  echo "I cannot find container $1?"
else
  docker inspect $aux | grep -w IPAddress | awk -F: '{ print $2 }' | xargs | awk -F\, '{ print $2}'
fi