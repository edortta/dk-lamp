# dk-lamp

#### Docker LAMP with web and mysql data persistence

The goal of this project is to deploy the essential tools to deal with a (l/w/m)AMP context through Docker.
It was not thought for whom has big experience with Docker.
It's intended to be used by single programmer or small group where time is a very limited resource.

### Installation

Using your console, go to your web project folder. Then run this:

    wget https://gitlab.com/edortta/dk-lamp/-/raw/main/install.sh -v -O install.sh  1> /dev/null 2> /dev/null && bash ./install.sh; rm -rf install.sh

### Usage

1. First you would like to adjust Dockerfile changing the packages that would be installed in the web server. Remember this project was originally intended to be used in Health and Fintech development.
2. Second, probably you'll want to check and maybe change docker-compose.yml in order to void tcp port collisions. That's specially true if you will use it with more than one project
3. After that, you will build your environment using `dkbuild.sh`. This will take a while the first time you run it.
4. Once it's built, you will want to compose the environment using `dkcompose.sh`
5. Test your project at http://localhost:8000.
6. Test phpmyadmin at http://localhost:8001.

### Available tools

*dklist.sh* returns a list of containers available in the project

*dkip.sh* returns the internal IP for the specified container

*dkshell.sh* allows you to enter into the specified container as when you use ssh

