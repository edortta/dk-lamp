#!/bin/bash
wd=`pwd`
base=`basename "$wd" | awk '{ print tolower($0) }'`
docker ps --format 'table {{.ID}}\t{{.Names}}\t{{.Status}}' | grep $base | awk '{ print $1 "\t" $2 }'