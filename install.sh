#!/bin/bash

echo "Downloading..."
for f in dkbuild.sh dkcompose.sh dkip.sh dklist.sh dkshell.sh docker-compose.yml Dockerfile
do
  echo "  $f"
  wget https://gitlab.com/edortta/dk-lamp/-/raw/main/$f -O $f  1> /dev/null 2> /dev/null
done
echo "Ready"