FROM ubuntu:latest
ARG DEBIAN_FRONTEND=noninteractive
RUN apt update -y 
RUN apt dist-upgrade -y 
RUN ln -fs /usr/share/zoneinfo/UTC /etc/localtime
RUN apt install apt-utils -y
RUN apt install apache2 apache2-utils php php-cli php-fpm php-json php-common php-mysql php-mysqli php-zip php-gd php-mbstring php-curl php-xml php-pear php-bcmath -y
RUN apt install libsodium-dev -y
RUN apt install tesseract-ocr -y
RUN apt install tesseract-ocr-por -y
RUN apt install imagemagick -y
RUN apt install -y iproute2 -y
RUN apt install certbot python3-certbot-apache -y
RUN apt install nano -y
RUN a2enmod rewrite
RUN a2enmod headers
RUN { \
  echo "<Directory /var/www/>" > /etc/apache2/conf-enabled/rewrite-module.conf; \
  echo "  Options Indexes FollowSymLinks" >> /etc/apache2/conf-enabled/rewrite-module.conf; \
  echo "  AllowOverride All" >> /etc/apache2/conf-enabled/rewrite-module.conf; \
  echo "  Require all granted" >> /etc/apache2/conf-enabled/rewrite-module.conf; \
  echo "</Directory>">> /etc/apache2/conf-enabled/rewrite-module.conf; \
  }
EXPOSE 80
ENTRYPOINT [ "/usr/sbin/apache2ctl" ]
CMD [ "-D", "FOREGROUND" ]
