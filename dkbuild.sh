#!/bin/bash
wd=`pwd`
base=`basename "$wd" | awk '{ print tolower($0) }'`
docker build -t "dev:$base" .